"use strict";
//let global = {};

document.addEventListener("DOMContentLoaded", setup);
//let i=0;//Error, does not restart to the beginning of quote after pressing start again
let button = document.querySelector('button');
let btnStart = false; //Represents whether the program is running or not
let interval;
let wpm;
let len;

function setup(){
    
    wpm=document.querySelector('#speed').value;

    
    let wpmSpeed = localStorage.getItem('wpmSpeed');
    if (wpmSpeed != 100){
        wpm = wpmSpeed;
        document.querySelector('#speed').value = wpm;
    }
    else if (wpmSpeed < 50){
        wpm = 100;
        document.querySelector('#speed').value = wpm;
    }
    localStorage.setItem('wpmSpeed',wpm);
    
    console.log(wpm);
    //document.querySelector('button');
    
   // console.log(button);
   button.addEventListener('click',status);
}

/**
 * @author Juan Corzo
 * @description fetch the Ron Swanson quotes and returns a promise that will then
 * be used ny the stringSplitter function
 */
function getQuote(){
    fetch('https://ron-swanson-quotes.herokuapp.com/v2/quotes')
    .then(response => {
        if (response.ok) {
            return response.json();
        }
        throw new Error('Status code: ' + response.status);
    })
    .then(json => stringSplitter(json))
    .catch(error => treatError(error))

}

/**
 * @author Kevin Echenique
 * @param {*} json 
 * @description Takes as parameter a JSON object, turns it into a string and
 * splits the sentence into a Stirng array. Lastly, it calls the displayQuote function 
 */
function stringSplitter(json){
    
    console.log(json);
    let sentence = JSON.stringify(json);
    let word = sentence.split(' ');
    len = word.length;

    console.log(word[len-1].slice(0,-2));

    word[len-1]=word[len-1].slice(0,-3);
    word[0]=word[0].slice(2);
    displayQuote(word);
}

/**
 * @author Kevin Echenique
 * @description This function will change the status (textContent) of the button
 * when the user clicks on it. It will display 'start' or 'stop' depending on its status
 */
function status(){

    console.log(button.textContent);
   
    //when the program is running
    if(btnStart == false){
        button.textContent = "Stop";
        getQuote();
        btnStart = true;
        //console.log(btnStart);
    }

    //when the program has stopped
    else if(btnStart == true){
        button.textContent = "Start";
        btnStart = false;

       clearInterval(interval);
       //console.log(btnStart); 
    }
        
}

/**
 * @author Kevin Echenique
 * @author Juan Corzo
 * @param {*} word
 * @description Takes a word as input (from the stringSplitter function) and displays it to the DOM.
 * We use an anonymous function inside the setInterval where we assign a highlighted character (a letter) which is gonna have a different color.
 * We also make this function responsible for the transition between quotes. 
 */
function displayQuote(word){
    let i = 0;
    let sect= document.querySelector('section');
    let para= document.querySelector('p');
    let before= document.querySelector('#char-before-focus');
    let focus= document.querySelector('#char-focus');
    let after= document.querySelector('#char-remaining');

    console.log(len);
    
    wpm=document.querySelector('#speed').value;
    interval=setInterval(function()
    {

        if(word[i].length==1){
            before.textContent='    ';
            focus.textContent=word[i];
            after.textContent='';  
        }
        if(word[i].length<=5 && word[i].length>=2){
            before.textContent='   '+word[i].substring(0,1);
            focus.textContent=word[i].substring(1,2);
            after.textContent=word[i].substring(2);  
        }
        if(word[i].length<=9 && word[i].length>=6){
            before.textContent='  '+word[i].substring(0,2);
            focus.textContent=word[i].substring(2,3);
            after.textContent=word[i].substring(3);  
        }
        if(word[i].length<=13 && word[i].length>=10){
            before.textContent=' '+word[i].substring(0,3);
            focus.textContent=word[i].substring(3,4);
            after.textContent=word[i].substring(4);  
        }
        if(word[i].length>13){
            before.textContent=word[i].substring(0,4);
            focus.textContent=word[i].substring(4,5);
            after.textContent=word[i].substring(5);  
        }
        if(i < len-1){
            i++;
        }
        else {
            
            clearInterval(interval);
            
            if(btnStart){
                i=0;
                getQuote();
            }
        }
    },60000/wpm);

    console.log(60000/wpm);
    
   /*
    para.textContent=word.slice(0,-2);
    sect.appendChild(para);
    console.log(sentence);*/
}

/**
 * @author Juan Corzo
 * @description catches an error when the response has a status code of 404 or is not ok
 * and prints an error message to the DOM
 */
function treatError(){
    let para= document.querySelector('p');
    para.textContent="Error: please try again";
}
